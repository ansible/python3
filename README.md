# Python3

Ansible role to install Python 3 and its apt package dependencies, and to
install pip3 and pip3 packages both for user and system-wide scope.


## Variables

Use the `pip_packages` list to define your pip packages for each of your hosts or groups.
Each item of this list is a dict with the following structure:
```
- name: <pip-package-name>  # mandatory
  install_scope: "system"   # optional, not defining it is the same as setting it to empty string
  install_args: ""          # optional, pip install <args>
  deps_apt: []              # optional, list of apt packages that the pip module depends on
```

The pip packages defined in `defaults` will be *merged* with your list of pip packages.
Note that the merge will *not* overwrite existing list items nor their properties
(for example, `{ name: setuptools, install_scope: "system" }` will not be affected
if you define `{ name: setuptools }` in your list).



## Links and notes

+ https://launchpad.net/~deadsnakes/+archive/ubuntu/ppa
+ https://askubuntu.com/questions/1398568/installing-python-who-is-deadsnakes-and-why-should-i-trust-them


### Other Ansible roles

+ https://github.com/mdklatt/ansible-python3-role
+ https://github.com/fubarhouse/ansible-role-python


### Something as conceptually simply as listing all package dependencies

Turns out to be immensely complicated for `pip` packages. No simple solution yet.
Maybe `pipdeptree --packages <pip-package>` does what we want?

+ https://stackoverflow.com/questions/11147667/is-there-a-way-to-list-pip-dependencies-requirements
+ https://www.activestate.com/resources/quick-reads/how-to-check-for-python-dependencies-with-popular-package-managers/
+ https://pip.pypa.io/en/latest/topics/dependency-resolution/


### Why we install pip packages as user, not root

I have moved away from installing `pip` and `pip` packages system-wide because
that lead to really hard-to-overcome clashes between `pip` packages and
`apt` packages, such as `pycairo`.

```
Attempting uninstall: pycairo
Found existing installation: pycairo 1.16.2
stderr: ERROR: Cannot uninstall 'pycairo'. It is a distutils installed project and thus we cannot accurately determine which files belong to it which would lead to only a partial uninstall
```

What if we *uninstall* `python3-cairo`?
```
$ sudo apt remove python3-cairo
Reading package lists... Done
Building dependency tree
Reading state information... Done
The following packages were automatically installed and are no longer required:
  fortune-mod fortunes-min gedit-common gir1.2-ayatanaappindicator3-0.1 gir1.2-gexiv2-0.10
  gir1.2-gtksource-3.0 libayatana-appindicator3-1 libayatana-indicator3-7 libgexiv2-2 libgspell-1-1
  libgspell-1-common libgtksourceview-3.0-1 libgtksourceview-3.0-common librecode0 python3-bs4
  python3-html5lib python3-lxml python3-olefile python3-pil python3-webencodings
Use 'sudo apt autoremove' to remove them.
The following packages will be REMOVED
  gedit meld python3-cairo python3-cairo-dev python3-gi-cairo variety
0 to upgrade, 0 to newly install, 6 to remove and 21 not to upgrade.
```
No, it is a dependency of multiple other packages and would break too much stuff.

Options?

+ simply power on by adding `--ignore-installed` to pip extra args,
  but that risks putting the entire system in an unmaintainable state
+ would it help if we changed `pip_user` from `root` to `{{ ansible_env.USER }}`?
  (we don't really need it to be root...)
+ if that does not help, then what?
+ install more `pip` packages in their own virtualenvs, instead of like now?
+ install python from source (a so-called private python installation), would that help?

Good commentary on this subject:
+ https://stackoverflow.com/questions/49932759/pip-10-and-apt-how-to-avoid-cannot-uninstall-x-errors-for-distutils-packages
+ https://stackoverflow.com/questions/53807511/pip-cannot-uninstall-package-it-is-a-distutils-installed-project/53807588#53807588
